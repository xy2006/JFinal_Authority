package com.jayqqaa12.system.controller;

import com.jayqqaa12.UrlConfig;
import com.jayqqaa12.jbase.jfinal.ext.Controller;
import com.jayqqaa12.jbase.util.L;
import com.jayqqaa12.shiro.ShiroCache;
import com.jayqqaa12.shiro.ShiroInterceptor;
import com.jayqqaa12.system.model.Res;
import com.jayqqaa12.system.validator.ResValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/res", viewPath = UrlConfig.SYSTEM)
public class ResController extends Controller
{

	public void tree()
	{
		int pid = getParaToInt("id", 0);
		int type = getParaToInt("type", Res.TYPE_MEUE);
		renderJson(Res.dao.getTree(pid, type));

	}

	public void list()
	{
		renderJson(Res.dao.list(" order by seq "));
	}

	public void delete()
	{
		renderJsonResult(Res.dao.deleteById(getPara("id")));
		removeAuthorization();
	}

	@Before(value = { ResValidator.class })
	public void add()
	{
		renderJsonResult(getModel(Res.class).emptyRemove("pid").save());
		removeAuthorization();
	}

	@Before(value = { ResValidator.class })
	public void edit()
	{
		renderJsonResult(getModel(Res.class).update());
		removeAuthorization();

	}
	
	
	
	private void removeAuthorization()
	{
		ShiroCache.clearAuthorizationInfoAll();
		ShiroInterceptor.updateUrls();
	}

}
