package com.jayqqaa12.system.controller;

import com.jayqqaa12.UrlConfig;
import com.jayqqaa12.jbase.jfinal.ext.Controller;
import com.jayqqaa12.jbase.util.L;
import com.jayqqaa12.shiro.ShiroCache;
import com.jayqqaa12.system.model.User;
import com.jayqqaa12.system.validator.UserValidator;
import com.jfinal.aop.Before;
import com.jfinal.ext.route.ControllerBind;

@ControllerBind(controllerKey = "/system/user", viewPath = UrlConfig.SYSTEM)
public class UserController extends Controller<User>
{

	public void list()
	{

		renderJson(User.dao.listByDataGrid(getDataGrid(), getFrom(User.dao.tableName)));
	}

	@Override
	public void delete()
	{
		renderJsonResult(User.dao.deleteById(getPara("id")));
	}

	public void batchDelete()
	{
		renderJsonResult(User.dao.batchDelete(getPara("ids")));
	}

	public void batchGrant()
	{

		Integer[] role_ids = getParaValuesToInt("role_ids");
		String ids = getPara("ids");

		renderJsonResult(User.dao.batchGrant(role_ids, ids));

		ShiroCache.clearAuthorizationInfoAll();

	}

	@Before(value = { UserValidator.class })
	public void add()
	{
		renderJsonResult(getModel(User.class).encrypt().saveAndDate());
	}

	@Override
	@Before(value = { UserValidator.class })
	public void edit()
	{
		renderJsonResult(getModel(User.class).encrypt().update());
	}

	public void grant()
	{
		Integer[] role_ids = getParaValuesToInt("role_ids");

		renderJsonResult(User.dao.grant(role_ids, getModel(User.class).getId()));

		ShiroCache.clearAuthorizationInfoAll();

	}

}
