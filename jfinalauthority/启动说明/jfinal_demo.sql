/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50162
Source Host           : localhost:3306
Source Database       : jfinal_demo

Target Server Type    : MYSQL
Target Server Version : 50162
File Encoding         : 65001

Date: 2014-04-02 13:04:58
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `index_msg`
-- ----------------------------
DROP TABLE IF EXISTS `index_msg`;
CREATE TABLE `index_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg` text,
  `date` datetime DEFAULT NULL,
  `uid` int(11) DEFAULT NULL COMMENT '留言用户 system_user',
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_MSG` (`uid`),
  CONSTRAINT `FK_SYSTEM_MSG` FOREIGN KEY (`uid`) REFERENCES `system_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COMMENT='留言板的 数据库';

-- ----------------------------
-- Records of index_msg
-- ----------------------------
INSERT INTO index_msg VALUES ('64', '222', '2014-03-28 13:27:44', '1');

-- ----------------------------
-- Table structure for `system_bug`
-- ----------------------------
DROP TABLE IF EXISTS `system_bug`;
CREATE TABLE `system_bug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) DEFAULT NULL,
  `des` text,
  `type` int(1) DEFAULT NULL COMMENT '类别',
  `createdate` datetime DEFAULT NULL,
  `modifydate` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1' COMMENT '1.待解决 2. 已处理 3.忽略',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_bug
-- ----------------------------
INSERT INTO system_bug VALUES ('2', '222', '<p>\r\n	<img src=\"http://api.map.baidu.com/staticimage?center=121.473704%2C31.230393&zoom=11&width=558&height=360&markers=121.473704%2C31.230393&markerStyles=l%2CA\" alt=\"\" />123123\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<img src=\"http://api.map.baidu.com/staticimage?center=121.473704%2C31.230393&zoom=11&width=558&height=360&markers=121.473704%2C31.230393&markerStyles=l%2CA\" alt=\"\" />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<br />\r\n</p>\r\n<p>\r\n	<img src=\"http://api.map.baidu.com/staticimage?center=121.473704%2C31.230393&zoom=11&width=558&height=360&markers=121.473704%2C31.230393&markerStyles=l%2CA\" alt=\"\" />\r\n</p>', '1', '2014-03-26 15:55:07', '2014-03-26 16:52:42', '1');

-- ----------------------------
-- Table structure for `system_log`
-- ----------------------------
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `operation` int(11) DEFAULT '0' COMMENT '1.访问 2 登录 3.添加 4. 编辑 5. 删除',
  `from` varchar(255) DEFAULT NULL COMMENT '来源 url',
  `ip` varchar(22) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_EVENT` (`uid`),
  CONSTRAINT `FK_SYSTEM_EVENT` FOREIGN KEY (`uid`) REFERENCES `system_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=604 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_log
-- ----------------------------
INSERT INTO system_log VALUES ('328', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-26 11:09:14');
INSERT INTO system_log VALUES ('329', null, '1', null, '127.0.0.1', '2014-03-26 11:09:25');
INSERT INTO system_log VALUES ('331', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-26 11:48:35');
INSERT INTO system_log VALUES ('333', null, '1', 'http://127.0.0.1:1234/system/log', '127.0.0.1', '2014-03-26 12:55:45');
INSERT INTO system_log VALUES ('335', null, '1', null, '127.0.0.1', '2014-03-26 14:36:07');
INSERT INTO system_log VALUES ('337', null, '1', 'http://127.0.0.1:1234/system/bug', '127.0.0.1', '2014-03-26 17:44:49');
INSERT INTO system_log VALUES ('339', null, '1', null, '127.0.0.1', '2014-03-27 11:22:34');
INSERT INTO system_log VALUES ('342', null, '1', null, '127.0.0.1', '2014-03-27 14:24:49');
INSERT INTO system_log VALUES ('344', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-27 15:13:42');
INSERT INTO system_log VALUES ('346', null, '1', null, '127.0.0.1', '2014-03-27 15:59:43');
INSERT INTO system_log VALUES ('348', null, '1', null, '127.0.0.1', '2014-03-27 16:23:26');
INSERT INTO system_log VALUES ('350', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-27 17:43:15');
INSERT INTO system_log VALUES ('351', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-27 17:46:04');
INSERT INTO system_log VALUES ('352', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-27 18:08:41');
INSERT INTO system_log VALUES ('353', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-27 18:08:44');
INSERT INTO system_log VALUES ('354', null, '1', null, '127.0.0.1', '2014-03-27 18:09:16');
INSERT INTO system_log VALUES ('355', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-27 18:09:18');
INSERT INTO system_log VALUES ('356', null, '1', null, '127.0.0.1', '2014-03-27 19:10:57');
INSERT INTO system_log VALUES ('358', '1', '5', 'http://127.0.0.1:1234/system/log', '127.0.0.1', '2014-03-27 19:11:08');
INSERT INTO system_log VALUES ('359', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-27 19:14:32');
INSERT INTO system_log VALUES ('360', null, '1', null, '127.0.0.1', '2014-03-28 10:16:38');
INSERT INTO system_log VALUES ('361', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-28 10:16:41');
INSERT INTO system_log VALUES ('362', null, '1', null, '127.0.0.1', '2014-03-28 13:25:29');
INSERT INTO system_log VALUES ('363', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-28 13:25:32');
INSERT INTO system_log VALUES ('364', '1', '3', 'http://127.0.0.1:1234/page/index/content.jsp', '127.0.0.1', '2014-03-28 13:27:44');
INSERT INTO system_log VALUES ('365', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 14:49:40');
INSERT INTO system_log VALUES ('366', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 14:50:18');
INSERT INTO system_log VALUES ('367', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 14:55:14');
INSERT INTO system_log VALUES ('368', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 14:56:10');
INSERT INTO system_log VALUES ('369', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:17:10');
INSERT INTO system_log VALUES ('370', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:17:22');
INSERT INTO system_log VALUES ('371', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:19:53');
INSERT INTO system_log VALUES ('372', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:19:57');
INSERT INTO system_log VALUES ('373', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:20:00');
INSERT INTO system_log VALUES ('374', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:20:08');
INSERT INTO system_log VALUES ('375', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:20:44');
INSERT INTO system_log VALUES ('376', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:20:47');
INSERT INTO system_log VALUES ('377', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:21:52');
INSERT INTO system_log VALUES ('378', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:23:54');
INSERT INTO system_log VALUES ('379', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:23:55');
INSERT INTO system_log VALUES ('380', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:26:16');
INSERT INTO system_log VALUES ('381', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:27:24');
INSERT INTO system_log VALUES ('382', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:29:01');
INSERT INTO system_log VALUES ('383', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:29:02');
INSERT INTO system_log VALUES ('384', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:29:05');
INSERT INTO system_log VALUES ('385', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:29:06');
INSERT INTO system_log VALUES ('386', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:29:06');
INSERT INTO system_log VALUES ('387', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:29:19');
INSERT INTO system_log VALUES ('388', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:30:07');
INSERT INTO system_log VALUES ('389', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:30:09');
INSERT INTO system_log VALUES ('390', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:30:33');
INSERT INTO system_log VALUES ('391', '1', '2', 'http://127.0.0.1:1234/login', '127.0.0.1', '2014-03-28 15:34:02');
INSERT INTO system_log VALUES ('392', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:34:17');
INSERT INTO system_log VALUES ('393', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:35:52');
INSERT INTO system_log VALUES ('394', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:45:28');
INSERT INTO system_log VALUES ('395', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:45:31');
INSERT INTO system_log VALUES ('396', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:45:55');
INSERT INTO system_log VALUES ('397', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:46:00');
INSERT INTO system_log VALUES ('398', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:46:02');
INSERT INTO system_log VALUES ('399', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 15:48:08');
INSERT INTO system_log VALUES ('400', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:11:55');
INSERT INTO system_log VALUES ('401', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:11:59');
INSERT INTO system_log VALUES ('402', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-28 16:12:39');
INSERT INTO system_log VALUES ('403', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:14:21');
INSERT INTO system_log VALUES ('404', '1', '5', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:14:24');
INSERT INTO system_log VALUES ('405', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:21:04');
INSERT INTO system_log VALUES ('406', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:23:17');
INSERT INTO system_log VALUES ('407', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:41:09');
INSERT INTO system_log VALUES ('408', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:41:10');
INSERT INTO system_log VALUES ('409', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:41:25');
INSERT INTO system_log VALUES ('410', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:50:38');
INSERT INTO system_log VALUES ('411', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 16:50:42');
INSERT INTO system_log VALUES ('412', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 17:03:01');
INSERT INTO system_log VALUES ('413', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 17:03:04');
INSERT INTO system_log VALUES ('414', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 17:03:09');
INSERT INTO system_log VALUES ('415', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-28 17:03:15');
INSERT INTO system_log VALUES ('416', null, '1', null, '127.0.0.1', '2014-03-28 17:13:49');
INSERT INTO system_log VALUES ('417', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-28 17:13:52');
INSERT INTO system_log VALUES ('418', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-28 17:43:00');
INSERT INTO system_log VALUES ('419', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-28 17:43:04');
INSERT INTO system_log VALUES ('420', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-28 17:43:48');
INSERT INTO system_log VALUES ('421', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-28 17:43:51');
INSERT INTO system_log VALUES ('422', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 09:06:43');
INSERT INTO system_log VALUES ('423', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 09:06:47');
INSERT INTO system_log VALUES ('424', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 09:30:56');
INSERT INTO system_log VALUES ('425', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 09:30:58');
INSERT INTO system_log VALUES ('426', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 10:11:16');
INSERT INTO system_log VALUES ('427', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 10:11:19');
INSERT INTO system_log VALUES ('428', null, '1', null, '127.0.0.1', '2014-03-29 10:25:38');
INSERT INTO system_log VALUES ('429', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 10:25:41');
INSERT INTO system_log VALUES ('430', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 10:27:44');
INSERT INTO system_log VALUES ('431', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 10:27:47');
INSERT INTO system_log VALUES ('432', null, '1', null, '127.0.0.1', '2014-03-29 11:16:39');
INSERT INTO system_log VALUES ('433', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 11:16:51');
INSERT INTO system_log VALUES ('434', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 11:56:26');
INSERT INTO system_log VALUES ('435', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 11:56:29');
INSERT INTO system_log VALUES ('436', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 12:57:17');
INSERT INTO system_log VALUES ('437', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 12:57:20');
INSERT INTO system_log VALUES ('438', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-29 13:14:48');
INSERT INTO system_log VALUES ('439', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-29 13:14:56');
INSERT INTO system_log VALUES ('440', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-29 13:15:35');
INSERT INTO system_log VALUES ('441', '1', '3', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-29 13:15:41');
INSERT INTO system_log VALUES ('442', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 13:25:22');
INSERT INTO system_log VALUES ('443', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 13:25:25');
INSERT INTO system_log VALUES ('444', null, '1', null, '127.0.0.1', '2014-03-29 13:42:52');
INSERT INTO system_log VALUES ('445', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 13:42:56');
INSERT INTO system_log VALUES ('446', '1', '3', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-29 13:44:13');
INSERT INTO system_log VALUES ('447', '1', '4', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-29 13:44:28');
INSERT INTO system_log VALUES ('448', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 13:44:46');
INSERT INTO system_log VALUES ('449', null, '1', null, '127.0.0.1', '2014-03-29 14:42:09');
INSERT INTO system_log VALUES ('450', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 14:42:13');
INSERT INTO system_log VALUES ('451', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 15:36:52');
INSERT INTO system_log VALUES ('452', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 15:36:56');
INSERT INTO system_log VALUES ('453', null, '1', 'http://127.0.0.1:1234/system/log', '127.0.0.1', '2014-03-29 16:02:54');
INSERT INTO system_log VALUES ('454', null, '1', 'http://127.0.0.1:1234/system/log', '127.0.0.1', '2014-03-29 16:03:09');
INSERT INTO system_log VALUES ('455', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 16:03:44');
INSERT INTO system_log VALUES ('456', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 16:13:18');
INSERT INTO system_log VALUES ('457', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 16:22:20');
INSERT INTO system_log VALUES ('458', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 16:22:23');
INSERT INTO system_log VALUES ('459', null, '1', 'http://127.0.0.1:1234/system/log', '127.0.0.1', '2014-03-29 16:31:59');
INSERT INTO system_log VALUES ('460', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 16:32:12');
INSERT INTO system_log VALUES ('461', null, '1', null, '192.168.149.38', '2014-03-29 16:39:16');
INSERT INTO system_log VALUES ('462', null, '1', null, '192.168.149.38', '2014-03-29 16:39:16');
INSERT INTO system_log VALUES ('463', '1', '2', 'http://192.168.149.38:1234/loginView', '192.168.149.38', '2014-03-29 16:39:24');
INSERT INTO system_log VALUES ('464', '1', '4', 'http://192.168.149.38:1234/system/user', '192.168.149.38', '2014-03-29 16:39:53');
INSERT INTO system_log VALUES ('465', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-29 16:40:23');
INSERT INTO system_log VALUES ('466', '1', '4', 'http://127.0.0.1:1234/system/role', '127.0.0.1', '2014-03-29 16:41:35');
INSERT INTO system_log VALUES ('467', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-29 16:47:31');
INSERT INTO system_log VALUES ('468', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 16:47:35');
INSERT INTO system_log VALUES ('469', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-29 16:48:06');
INSERT INTO system_log VALUES ('470', '1', '3', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-29 17:16:30');
INSERT INTO system_log VALUES ('471', '1', '5', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-29 17:16:50');
INSERT INTO system_log VALUES ('472', null, '1', null, '127.0.0.1', '2014-03-29 17:42:00');
INSERT INTO system_log VALUES ('473', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-29 17:42:03');
INSERT INTO system_log VALUES ('474', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 09:15:53');
INSERT INTO system_log VALUES ('475', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 09:15:57');
INSERT INTO system_log VALUES ('476', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 09:36:55');
INSERT INTO system_log VALUES ('477', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 09:36:57');
INSERT INTO system_log VALUES ('478', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 09:37:09');
INSERT INTO system_log VALUES ('479', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 09:37:18');
INSERT INTO system_log VALUES ('480', '1', '4', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 09:37:29');
INSERT INTO system_log VALUES ('481', '13', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 09:37:42');
INSERT INTO system_log VALUES ('482', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 09:38:03');
INSERT INTO system_log VALUES ('483', '1', '3', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:03:33');
INSERT INTO system_log VALUES ('484', '1', '3', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:10:20');
INSERT INTO system_log VALUES ('485', '1', '3', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:10:46');
INSERT INTO system_log VALUES ('486', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:14:06');
INSERT INTO system_log VALUES ('487', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 10:14:09');
INSERT INTO system_log VALUES ('488', '1', '5', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:14:14');
INSERT INTO system_log VALUES ('489', '1', '3', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:14:23');
INSERT INTO system_log VALUES ('490', '1', '5', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:14:27');
INSERT INTO system_log VALUES ('491', null, '1', null, '127.0.0.1', '2014-03-31 10:18:53');
INSERT INTO system_log VALUES ('492', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 10:18:56');
INSERT INTO system_log VALUES ('493', '1', '5', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:21:46');
INSERT INTO system_log VALUES ('494', '1', '3', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:39:29');
INSERT INTO system_log VALUES ('495', '1', '4', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 10:40:31');
INSERT INTO system_log VALUES ('496', null, '1', null, '127.0.0.1', '2014-03-31 10:43:11');
INSERT INTO system_log VALUES ('497', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 10:43:13');
INSERT INTO system_log VALUES ('498', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:43:42');
INSERT INTO system_log VALUES ('499', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:05');
INSERT INTO system_log VALUES ('500', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:09');
INSERT INTO system_log VALUES ('501', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:18');
INSERT INTO system_log VALUES ('502', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:21');
INSERT INTO system_log VALUES ('503', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:23');
INSERT INTO system_log VALUES ('504', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:24');
INSERT INTO system_log VALUES ('505', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:25');
INSERT INTO system_log VALUES ('506', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:27');
INSERT INTO system_log VALUES ('507', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:29');
INSERT INTO system_log VALUES ('508', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:33');
INSERT INTO system_log VALUES ('509', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:34');
INSERT INTO system_log VALUES ('510', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:44:34');
INSERT INTO system_log VALUES ('511', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 10:49:21');
INSERT INTO system_log VALUES ('512', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 10:51:38');
INSERT INTO system_log VALUES ('513', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 10:52:01');
INSERT INTO system_log VALUES ('514', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:52:11');
INSERT INTO system_log VALUES ('515', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 10:52:15');
INSERT INTO system_log VALUES ('516', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:52:27');
INSERT INTO system_log VALUES ('517', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:53:41');
INSERT INTO system_log VALUES ('518', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:55:17');
INSERT INTO system_log VALUES ('519', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 10:55:32');
INSERT INTO system_log VALUES ('520', null, '1', null, '192.168.149.38', '2014-03-31 11:05:16');
INSERT INTO system_log VALUES ('521', null, '1', null, '192.168.149.38', '2014-03-31 11:05:49');
INSERT INTO system_log VALUES ('522', null, '1', null, '192.168.149.43', '2014-03-31 11:05:56');
INSERT INTO system_log VALUES ('523', null, '1', null, '192.168.149.43', '2014-03-31 11:05:56');
INSERT INTO system_log VALUES ('524', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:11:58');
INSERT INTO system_log VALUES ('525', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:12:03');
INSERT INTO system_log VALUES ('526', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:12:53');
INSERT INTO system_log VALUES ('527', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:14:16');
INSERT INTO system_log VALUES ('528', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:14:20');
INSERT INTO system_log VALUES ('529', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:14:39');
INSERT INTO system_log VALUES ('530', '1', '4', 'http://127.0.0.1:1234/system/role', '127.0.0.1', '2014-03-31 11:14:46');
INSERT INTO system_log VALUES ('531', '1', '4', 'http://127.0.0.1:1234/system/role', '127.0.0.1', '2014-03-31 11:14:49');
INSERT INTO system_log VALUES ('532', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:18:01');
INSERT INTO system_log VALUES ('533', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:19:48');
INSERT INTO system_log VALUES ('534', '1', '6', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 11:19:50');
INSERT INTO system_log VALUES ('535', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:20:13');
INSERT INTO system_log VALUES ('537', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:20:15');
INSERT INTO system_log VALUES ('538', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:20:18');
INSERT INTO system_log VALUES ('539', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:20:18');
INSERT INTO system_log VALUES ('540', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:20:39');
INSERT INTO system_log VALUES ('541', '1', '5', 'http://127.0.0.1:1234/system/log', '127.0.0.1', '2014-03-31 11:21:16');
INSERT INTO system_log VALUES ('542', null, '1', null, '127.0.0.1', '2014-03-31 11:23:52');
INSERT INTO system_log VALUES ('543', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:23:56');
INSERT INTO system_log VALUES ('544', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:25:34');
INSERT INTO system_log VALUES ('545', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:26:27');
INSERT INTO system_log VALUES ('546', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:26:35');
INSERT INTO system_log VALUES ('547', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:26:35');
INSERT INTO system_log VALUES ('548', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:26:36');
INSERT INTO system_log VALUES ('549', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:26:40');
INSERT INTO system_log VALUES ('550', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:26:41');
INSERT INTO system_log VALUES ('551', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:26:54');
INSERT INTO system_log VALUES ('552', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:27:51');
INSERT INTO system_log VALUES ('553', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:29:03');
INSERT INTO system_log VALUES ('554', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:30:32');
INSERT INTO system_log VALUES ('555', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:30:43');
INSERT INTO system_log VALUES ('556', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:31:33');
INSERT INTO system_log VALUES ('557', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:31:51');
INSERT INTO system_log VALUES ('558', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:32:14');
INSERT INTO system_log VALUES ('559', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:32:15');
INSERT INTO system_log VALUES ('560', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:32:31');
INSERT INTO system_log VALUES ('561', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:35:33');
INSERT INTO system_log VALUES ('562', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:35:37');
INSERT INTO system_log VALUES ('563', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:35:40');
INSERT INTO system_log VALUES ('564', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:35:41');
INSERT INTO system_log VALUES ('565', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:35:41');
INSERT INTO system_log VALUES ('566', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:35:54');
INSERT INTO system_log VALUES ('567', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:36:15');
INSERT INTO system_log VALUES ('568', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:38:47');
INSERT INTO system_log VALUES ('569', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:38:49');
INSERT INTO system_log VALUES ('570', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:38:53');
INSERT INTO system_log VALUES ('571', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:38:54');
INSERT INTO system_log VALUES ('572', null, '1', null, '127.0.0.1', '2014-03-31 11:39:27');
INSERT INTO system_log VALUES ('573', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:39:43');
INSERT INTO system_log VALUES ('574', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:39:53');
INSERT INTO system_log VALUES ('575', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:40:57');
INSERT INTO system_log VALUES ('576', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:40:59');
INSERT INTO system_log VALUES ('577', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:01');
INSERT INTO system_log VALUES ('578', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:04');
INSERT INTO system_log VALUES ('579', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:05');
INSERT INTO system_log VALUES ('580', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:09');
INSERT INTO system_log VALUES ('581', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:12');
INSERT INTO system_log VALUES ('582', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:17');
INSERT INTO system_log VALUES ('583', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:18');
INSERT INTO system_log VALUES ('584', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:19');
INSERT INTO system_log VALUES ('585', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:41:19');
INSERT INTO system_log VALUES ('586', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:43:09');
INSERT INTO system_log VALUES ('587', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:26');
INSERT INTO system_log VALUES ('588', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:30');
INSERT INTO system_log VALUES ('589', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:32');
INSERT INTO system_log VALUES ('590', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:33');
INSERT INTO system_log VALUES ('591', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:33');
INSERT INTO system_log VALUES ('592', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:34');
INSERT INTO system_log VALUES ('593', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:34');
INSERT INTO system_log VALUES ('594', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:34');
INSERT INTO system_log VALUES ('595', '1', '6', 'http://127.0.0.1:1234/system/user', '127.0.0.1', '2014-03-31 11:43:35');
INSERT INTO system_log VALUES ('596', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 11:44:48');
INSERT INTO system_log VALUES ('597', '1', '4', 'http://127.0.0.1:1234/system/res', '127.0.0.1', '2014-03-31 11:46:46');
INSERT INTO system_log VALUES ('598', null, '1', 'http://127.0.0.1:1234/', '127.0.0.1', '2014-03-31 12:38:18');
INSERT INTO system_log VALUES ('599', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-03-31 12:38:21');
INSERT INTO system_log VALUES ('600', null, '1', null, '127.0.0.1', '2014-04-02 09:57:36');
INSERT INTO system_log VALUES ('601', '1', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-04-02 09:57:40');
INSERT INTO system_log VALUES ('602', '1', '2', 'http://127.0.0.1:1234/login', '127.0.0.1', '2014-04-02 09:58:04');
INSERT INTO system_log VALUES ('603', '13', '2', 'http://127.0.0.1:1234/loginView', '127.0.0.1', '2014-04-02 09:58:15');

-- ----------------------------
-- Table structure for `system_res`
-- ----------------------------
DROP TABLE IF EXISTS `system_res`;
CREATE TABLE `system_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0',
  `name` varchar(111) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `iconCls` varchar(255) DEFAULT 'wrench',
  `seq` int(11) DEFAULT '1',
  `type` int(1) DEFAULT '2' COMMENT '1 功能 2 权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_res
-- ----------------------------
INSERT INTO system_res VALUES ('1', '0', '系统管理', '系统管理', null, 'plugin', '10', '1');
INSERT INTO system_res VALUES ('2', '1', '资源管理', null, '/system/res', 'database_gear', '1', '1');
INSERT INTO system_res VALUES ('3', '1', '角色管理', null, '/system/role', 'tux', '10', '1');
INSERT INTO system_res VALUES ('4', '1', '用户管理', null, '/system/user', 'status_online', '11', '1');
INSERT INTO system_res VALUES ('5', '1', '数据库管理', null, '/druid', 'database', '14', '1');
INSERT INTO system_res VALUES ('6', '1', '系统监控', null, '/monitoring', 'database', '15', '1');
INSERT INTO system_res VALUES ('7', '1', 'bug管理', null, '/system/bug', 'bug', '12', '1');
INSERT INTO system_res VALUES ('8', '4', '用户添加', null, '/system/user/add', 'wrench', '1', '2');
INSERT INTO system_res VALUES ('9', '4', '用户删除', null, '/system/user/delete', 'wrench', '1', '2');
INSERT INTO system_res VALUES ('10', '4', '用户编辑', null, '/system/user/edit', 'wrench', '1', '2');
INSERT INTO system_res VALUES ('12', '4', '搜索用户', null, '/system/user/serach', 'wrench', '1', '2');
INSERT INTO system_res VALUES ('13', '4', '批量授权', null, '/system/user/batchGrant', 'wrench', '1', '2');
INSERT INTO system_res VALUES ('14', '4', '批量删除', null, '/system/user/batchDelete', 'wrench', '1', '2');
INSERT INTO system_res VALUES ('15', '4', '用户授权', null, '/system/user/grant', 'wrench', '1', '2');
INSERT INTO system_res VALUES ('16', '0', '项目地址', null, 'http://git.oschina.net/jayqqaa12/JFinal_Authority', 'github', '15', '1');
INSERT INTO system_res VALUES ('17', '1', '日志管理', null, '/system/log', 'page_edit', '11', '1');
INSERT INTO system_res VALUES ('18', '2', '资源删除', null, '/system/res/delete', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('19', '2', '资源添加', null, '/system/res/add', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('20', '2', '资源编辑', null, '/system/res/edit', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('27', '3', '角色添加', null, '/system/role/add', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('28', '3', '角色删除', null, '/system/role/delete', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('29', '3', '角色编辑', null, '/system/role/edit', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('30', '3', '权限管理', null, '/system/role/grant', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('31', '7', 'bug添加', null, '/system/bug/add', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('32', '17', '日志删除', null, '/system/log/delete', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('33', '17', '查看统计图', null, '/system/log/chart', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('34', '7', 'bug删除', null, '/system/bug/delete', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('35', '7', 'bug编辑', null, '/system/bug/edit', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('36', '1', '文件上传', null, '/common/file/upload', 'wrench', '20', '2');
INSERT INTO system_res VALUES ('39', '7', 'bug查看', null, '/system/bug/view', 'wrench', '13', '2');
INSERT INTO system_res VALUES ('40', '17', '导出Excel', null, '/system/log/excel', 'wrench', '11', '2');
INSERT INTO system_res VALUES ('47', '1', '动态图表', null, '/system/chart', 'server_chart', '21', '1');

-- ----------------------------
-- Table structure for `system_role`
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `des` varchar(55) DEFAULT NULL,
  `seq` int(11) DEFAULT '1',
  `iconCls` varchar(55) DEFAULT 'status_online',
  `pid` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role
-- ----------------------------
INSERT INTO system_role VALUES ('1', 'admin', '管理员', '1', 'status_online', '0');
INSERT INTO system_role VALUES ('2', 'user', null, '3', 'status_online', '1');
INSERT INTO system_role VALUES ('3', 'guest', '临时用户', '2', 'status_online', '0');

-- ----------------------------
-- Table structure for `system_role_res`
-- ----------------------------
DROP TABLE IF EXISTS `system_role_res`;
CREATE TABLE `system_role_res` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `res_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_ROLE_RES_RES_ID` (`res_id`),
  KEY `FK_SYSTEM_ROLE_RES_ROLE_ID` (`role_id`),
  CONSTRAINT `FK_SYSTEM_ROLE_RES_RES_ID` FOREIGN KEY (`res_id`) REFERENCES `system_res` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SYSTEM_ROLE_RES_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `system_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1760 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role_res
-- ----------------------------
INSERT INTO system_role_res VALUES ('1225', '18', '3');
INSERT INTO system_role_res VALUES ('1226', '19', '3');
INSERT INTO system_role_res VALUES ('1227', '20', '3');
INSERT INTO system_role_res VALUES ('1670', '1', '1');
INSERT INTO system_role_res VALUES ('1671', '2', '1');
INSERT INTO system_role_res VALUES ('1672', '18', '1');
INSERT INTO system_role_res VALUES ('1673', '19', '1');
INSERT INTO system_role_res VALUES ('1674', '20', '1');
INSERT INTO system_role_res VALUES ('1675', '3', '1');
INSERT INTO system_role_res VALUES ('1676', '27', '1');
INSERT INTO system_role_res VALUES ('1677', '28', '1');
INSERT INTO system_role_res VALUES ('1678', '29', '1');
INSERT INTO system_role_res VALUES ('1679', '30', '1');
INSERT INTO system_role_res VALUES ('1680', '4', '1');
INSERT INTO system_role_res VALUES ('1681', '8', '1');
INSERT INTO system_role_res VALUES ('1682', '9', '1');
INSERT INTO system_role_res VALUES ('1683', '10', '1');
INSERT INTO system_role_res VALUES ('1684', '12', '1');
INSERT INTO system_role_res VALUES ('1685', '13', '1');
INSERT INTO system_role_res VALUES ('1686', '14', '1');
INSERT INTO system_role_res VALUES ('1687', '15', '1');
INSERT INTO system_role_res VALUES ('1688', '17', '1');
INSERT INTO system_role_res VALUES ('1689', '32', '1');
INSERT INTO system_role_res VALUES ('1690', '33', '1');
INSERT INTO system_role_res VALUES ('1691', '40', '1');
INSERT INTO system_role_res VALUES ('1692', '7', '1');
INSERT INTO system_role_res VALUES ('1693', '31', '1');
INSERT INTO system_role_res VALUES ('1694', '34', '1');
INSERT INTO system_role_res VALUES ('1695', '35', '1');
INSERT INTO system_role_res VALUES ('1696', '39', '1');
INSERT INTO system_role_res VALUES ('1697', '5', '1');
INSERT INTO system_role_res VALUES ('1698', '6', '1');
INSERT INTO system_role_res VALUES ('1699', '36', '1');
INSERT INTO system_role_res VALUES ('1700', '47', '1');
INSERT INTO system_role_res VALUES ('1701', '16', '1');
INSERT INTO system_role_res VALUES ('1728', '1', '2');
INSERT INTO system_role_res VALUES ('1729', '2', '2');
INSERT INTO system_role_res VALUES ('1730', '18', '2');
INSERT INTO system_role_res VALUES ('1731', '19', '2');
INSERT INTO system_role_res VALUES ('1732', '20', '2');
INSERT INTO system_role_res VALUES ('1733', '3', '2');
INSERT INTO system_role_res VALUES ('1734', '27', '2');
INSERT INTO system_role_res VALUES ('1735', '28', '2');
INSERT INTO system_role_res VALUES ('1736', '29', '2');
INSERT INTO system_role_res VALUES ('1737', '30', '2');
INSERT INTO system_role_res VALUES ('1738', '4', '2');
INSERT INTO system_role_res VALUES ('1739', '8', '2');
INSERT INTO system_role_res VALUES ('1740', '9', '2');
INSERT INTO system_role_res VALUES ('1741', '10', '2');
INSERT INTO system_role_res VALUES ('1742', '12', '2');
INSERT INTO system_role_res VALUES ('1743', '13', '2');
INSERT INTO system_role_res VALUES ('1744', '14', '2');
INSERT INTO system_role_res VALUES ('1745', '15', '2');
INSERT INTO system_role_res VALUES ('1746', '17', '2');
INSERT INTO system_role_res VALUES ('1747', '32', '2');
INSERT INTO system_role_res VALUES ('1748', '33', '2');
INSERT INTO system_role_res VALUES ('1749', '40', '2');
INSERT INTO system_role_res VALUES ('1750', '7', '2');
INSERT INTO system_role_res VALUES ('1751', '31', '2');
INSERT INTO system_role_res VALUES ('1752', '34', '2');
INSERT INTO system_role_res VALUES ('1753', '35', '2');
INSERT INTO system_role_res VALUES ('1754', '39', '2');
INSERT INTO system_role_res VALUES ('1755', '5', '2');
INSERT INTO system_role_res VALUES ('1756', '6', '2');
INSERT INTO system_role_res VALUES ('1757', '36', '2');
INSERT INTO system_role_res VALUES ('1758', '47', '2');
INSERT INTO system_role_res VALUES ('1759', '16', '2');

-- ----------------------------
-- Table structure for `system_user`
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `des` varchar(55) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '#1 不在线 2 在线 3.封号状态 ',
  `icon` varchar(255) DEFAULT '/images/guest.jpg',
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO system_user VALUES ('1', 'jayqqaa12', '0EECD063CDB4D838E03A56555D86A9AF', '12', '1', '/images/guest.jpg', '2014-01-12 17:18:57');
INSERT INTO system_user VALUES ('13', 'user', 'F59BD65F7EDAFB087A81D4DCA06C4910', '12', '1', '/images/guest.jpg', '2014-03-28 17:03:04');
INSERT INTO system_user VALUES ('14', '12321', '58ACD4036758EC886CC3C6351E17DC3F', null, '1', '/images/guest.jpg', '2014-03-29 13:14:48');

-- ----------------------------
-- Table structure for `system_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role`;
CREATE TABLE `system_user_role` (
  `int` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`int`),
  KEY `FK_SYSTME_USER_ROLE_USER_ID` (`user_id`),
  KEY `FK_SYSTME_USER_ROLE_ROLE_ID` (`role_id`),
  CONSTRAINT `FK_SYSTME_USER_ROLE_ROLE_ID` FOREIGN KEY (`role_id`) REFERENCES `system_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SYSTME_USER_ROLE_USER_ID` FOREIGN KEY (`user_id`) REFERENCES `system_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user_role
-- ----------------------------
INSERT INTO system_user_role VALUES ('95', '13', '2');
INSERT INTO system_user_role VALUES ('96', '13', '3');
INSERT INTO system_user_role VALUES ('97', '14', '2');
INSERT INTO system_user_role VALUES ('98', '14', '3');
INSERT INTO system_user_role VALUES ('105', '1', '1');
